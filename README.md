# ![RealWorld Example App](images/spring-logo.png)

## Drill4J Integration Steps

Pre-requirements:

- Gradle
- Java 11

To receive coverage report from Drill4J you have to:

1. Add to your  `build.gradle` file drill-ci-cd-integration plugin. That plugin will be used to generate comment to your
   merge request.

```groovy
plugins {
    id("com.epam.drill.integration.cicd") version "0.0.1"
}
```

Add general properties to your Gradle build file:

```groovy
drillCiCd {
    groupId = "realworld" //Drill4J group ID.
    appId = "realworld-backend" //Drill4J application ID.
    drillApiUrl = "http://localhost:8090/api" //Drill4J API url.
    drillApiKey = System.getenv("DRILL_API_KEY") //Drill4J Api Key.
    gitlab {
        it.projectId = System.getenv("CI_PROJECT_ID") // Project ID taken from gitlab env vars.     
        it.apiUrl = "https://gitlab.com/api" //Gitlab API url.
        it.privateToken = System.getenv("DRILL_BOT_GITLAB_TOKEN") //Gitlab API Private Token.
    }
}
```

2. Add to your `build.gradle` file autotest-agent plugin. That plugin will be used when api,unit tests will run.

```groovy
plugins {
    id('com.epam.drill.autotest.runner') version '0.23.0'
}
```

Add general properties to your Gradle build file:

```groovy
drill {
    drillApiUrl = System.getenv("DRILL_API_URL") //Drill4J API url.
    drillApiKey = System.getenv("DRILL_API_KEY") //Drill4J Api Key.
    groupId = "realworld" // Drill4J group ID.
    appId = "spring-webflux" //Drill4J application ID.
}
```

3. Set-up env.variables for repo:

- **DRILL_API_KEY** - api-key to access Drill4J server;
- **DRILL_API_URL** - url that points to Drill4J server;
- **DRILL_BOT_GITLAB_TOKEN** - gitlab token. [Here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
  is description how to get it;
- **JAVA_AGENT_URL** - url to download java-agent. All available version
  check [here](https://github.com/Drill4J/java-agent/releases)

4. Configure `.gitlab-ci.yml` file:

For main branch add to your pipline jobs:

- **sendBuildInfo** task, that will send all needed info to Drill4J to compare main and feature branches. See example
  below:

```yaml
build:
  # some configuration
  script:
    - ./gradlew clean assemble
    #some operations...
    - ./gradlew drillSendBuildInfo
```

**_NOTE_**: if you use alpine (or custom\other) image, do not forget to add git package. Show example for alpine image:

```yaml
before-script:
  - apk add --no-cache git
```

- **report_by_branch** job will attach as an artifact to your pipline .zip file with coverage report:

 ```yaml
stages:
  - report_by_branch

report_by_branch:
  stage: report_by_branch
  image: openjdk:11
  before_script:
    - apt-get install git
    - chmod +x ./gradlew
  script:
    - ./gradlew drillGenerateChangeTestingReport
  artifacts:
    paths:
      - ./build/reports/drill/drillReport.md # Path to generated report file.
```

* For feature branches add to your pipline job:
  **drillGitlabMergeRequestReport** job will create at merge request comment with coverage info:

```yaml
report_by_pr:
  stage: report_by_pr
  image: openjdk:11
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  before_script:
    - apt-get install git
    - chmod +x ./gradlew
  script:
    - ./gradlew drillGitlabMergeRequestReport
```

### Test running

To collect info about your build you will need to configure test stage. See example below:

```yaml
test:
  stage: test
  image: docker:latest
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
  services:
    - name: docker:dind
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - apk add --no-cache curl
    - mkdir ./Docker/agent
  script:
    - curl -L $JAVA_AGENT_URL -o agent.zip # Download Drill4J java-agent.
    - unzip agent.zip -d ./Docker/agent/
    - docker-compose -f ./Docker/docker-compose.yml up --exit-code-from api-tests # Run api tests.
```

In that demo we are using docker containers:

1. Realworld API backend container. Docker image is described at [Dockerfile-api-backend](Dockerfile-api-backend)
2. API tests container. Docker image that is described at [Dockerfile-api-tests](Dockerfile-api-tests).

Both of that containers are using at [docker-compose.yml](Docker%2Fdocker-compose.yml) file. Make a notes, that you have
to add\extend `JAVA_TOOL_OPTIONS` for backend container with Drill4J agent params:

```yaml
      - "JAVA_TOOL_OPTIONS=-agentpath:/data/agent/libdrill_agent.so=drillApiUrl=${DRILL_API_URL},appId=spring-webflux,groupId=realworld,drillApiKey=${DRILL_API_KEY},packagePrefixes=com/realworld,logLevel=INFO,commitSha=${CI_COMMIT_SHA}"
```

Also you have to mount volume with Drill4J java-agent. At that workflow java-agent will be downloaded from
the link provided at `JAVA_AGENT_URL`. See example below:

```yaml
    volumes:
      - ./agent/linuxX64-0.9.0/:/data/agent
```

**_NOTE_**: linuxX64-0.9.0 can be changed by version that you defined at `JAVA_AGENT_URL`

**Reminder**: At the `JAVA_TOOL_OPTIONS` do not forget to change:

- `groupId`: Drill4J group ID;
- `appId`: Drill4J application ID;
- `packagePrefixes`: path to your classes (equals to package name).

That [docker-compose.yml](Docker%2Fdocker-compose.yml) file is using at the test stage. See example below:

### Testing workflow

1. Build main branch to send metadata about it to Drill4J;
2. Wait until job will finish.
3. Create feature branch from main;
4. Push some changes to GitLab;
5. Create merge request;
6. Wait until job will finish.

As a result you will get:

- Change testing report from Drill4J:

![report-example.png](images/report-example.png)

- Artifact with branch coverage report attached to pipline of main branch:

![artifact-report-example.png](images/artifact-report-example.png)
